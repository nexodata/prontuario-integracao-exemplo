# Integração NexoData Receitas

Esse projeto em React.js tem como objetivo ajudar os desenvolvedores a integrarem uma tela de  prescrição eletrônica mais segura e user friendly em seu sistema.

Podendo analisar o código fonte da aplicação, ficando mais intuitivo de entender como pode ser adaptado para seu uso específico.

## Como rodar o projeto?


Instalando as depêndecias:
    
    $ yarn install

ou 

    $ npm install --save

Inicializando o projeto:

    $ yarn start

ou 

    $ npm start

![Tela da Aplicação](./tela.png)


## Como conseguir um token de acesso?

Entrar em contato com:

*parceiros@nexodata.com.br*


Vamos juntos mudar a saúde no Brasil,

__Happy coding!__

Nexodata
Driving Healthcare Forward 