import axios from "axios";

export const CEPService = {
  async searchForCepInViaCep(cep) {
    const { data } = await axios.get(`https://viacep.com.br/ws/${cep}/json/`);
    return data;
  },
};
